import scroller from './scroller'
import anchors from './anchors'
import buttons from './buttons'
import docs from './docs'

scroller.setup()
anchors.setup()
buttons.setup()
docs.setup()
